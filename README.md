1.git clone https://Ing.Ennis@gitlab.com/Ing.Ennis/GeoPago.git

2.Estando desde la consola en la carpeta del proyecto: php composer.phar install

3.Configurar la base de datos en: app/parameters.yml
3.1 En parameters.yml en (database_name) poner figuras

4.Creamos la bd: php bin/console doctrine:database:create

5.Creamos el schema: php bin/console doctrine:schema:create
5.1 En la gestor de BD ejecutar esta consulta:
    use (nombre de BD que configuro en parameters.yml) figuras
    INSERT into type_figura (nombre) VALUES ('Triangulo'),('Cuadrado'),('Circulo')
6. php bin/console asstes:install --symlink
6.Levantamos el servidor: php bin/console server:run

7.En el navegador debemos poner la dirección que nos muestra:  http://127.0.0.1:8000/