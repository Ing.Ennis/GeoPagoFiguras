<?php

namespace FigurasBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FigurasBundle\Services\Factory\FigurasFactory;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FigurasController extends Controller
{
    public function indexAction(){

        return $this->render('FigurasBundle:Default:index.html.twig');

    }
    public function getFiguraAction(){
        $service = $this->container->get('figura.service');
        $figura = $service->allFigura();
        return new JsonResponse($figura);
    }
    public function postAction(Request $request)
    {
        try{
            $parameters = $request->request->all();
            $factory = $this->container->get('figura.factory');
            $service = $this->container->get('figura.service');
            $figura = $factory->createFigura($parameters);
            $service->addFigura($figura);
            return $this->redirect($this->generateUrl('figuras_homepage'));
        }catch (Exception $e){
            return $this->redirect($this->generateUrl('figuras_homepage',array('error'=>$e->getMessage())));

        }
    }

}
