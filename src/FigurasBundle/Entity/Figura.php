<?php

namespace FigurasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Figura
 *
 * @ORM\Table(name="figura", indexes={@ORM\Index(name="fk_figura_1_idx", columns={"type"})})
 * @ORM\Entity
 */
class Figura
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="superficie", type="string", length=45, nullable=true)
     */
    private $superficie;

    /**
     * @var string
     *
     * @ORM\Column(name="base", type="string", length=45, nullable=true)
     */
    private $base;

    /**
     * @var string
     *
     * @ORM\Column(name="altura", type="string", length=45, nullable=true)
     */
    private $altura;

    /**
     * @var string
     *
     * @ORM\Column(name="radio", type="string", length=45, nullable=true)
     */
    private $radio;

    /**
     * @var string
     *
     * @ORM\Column(name="diametro", type="string", length=45, nullable=true)
     */
    private $diametro;

    /**
     * @var \TypeFigura
     *
     * @ORM\ManyToOne(targetEntity="TypeFigura")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="type", referencedColumnName="id")
     * })
     */
    private $type;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set superficie
     *
     * @param string $superficie
     *
     * @return Figura
     */
    public function setSuperficie($superficie)
    {
        $this->superficie = $superficie;

        return $this;
    }

    /**
     * Get superficie
     *
     * @return string
     */
    public function getSuperficie()
    {
        return $this->superficie;
    }

    /**
     * Set base
     *
     * @param string $base
     *
     * @return Figura
     */
    public function setBase($base)
    {
        $this->base = $base;

        return $this;
    }

    /**
     * Get base
     *
     * @return string
     */
    public function getBase()
    {
        return $this->base;
    }

    /**
     * Set altura
     *
     * @param string $altura
     *
     * @return Figura
     */
    public function setAltura($altura)
    {
        $this->altura = $altura;

        return $this;
    }

    /**
     * Get altura
     *
     * @return string
     */
    public function getAltura()
    {
        return $this->altura;
    }

    /**
     * Set radio
     *
     * @param string $radio
     *
     * @return Figura
     */
    public function setRadio($radio)
    {
        $this->radio = $radio;

        return $this;
    }

    /**
     * Get radio
     *
     * @return string
     */
    public function getRadio()
    {
        return $this->radio;
    }

    /**
     * Set diametro
     *
     * @param string $diametro
     *
     * @return Figura
     */
    public function setDiametro($diametro)
    {
        $this->diametro = $diametro;

        return $this;
    }

    /**
     * Get diametro
     *
     * @return string
     */
    public function getDiametro()
    {
        return $this->diametro;
    }

    /**
     * Set type
     *
     * @param \FigurasBundle\Entity\TypeFigura $type
     *
     * @return Figura
     */
    public function setType(\FigurasBundle\Entity\TypeFigura $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \FigurasBundle\Entity\TypeFigura
     */
    public function getType()
    {
        return $this->type;
    }
    public function getData(){
        $objFigura = new \stdClass();
        $objFigura->nombre = $this->type->getNombre();
        $objFigura->diametro = $this->diametro;
        $objFigura->base = $this->base;
        $objFigura->superficie = $this->superficie;
        $objFigura->altura = $this->altura;
        return $objFigura;
    }
}
