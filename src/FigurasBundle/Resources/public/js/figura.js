/**
 * Created by yanet on 13/03/2017.
 */
$(document).ready(function () {
    $('select#selectFigura').change(function () {

        $('#'+1).hide();
        $('#'+2).hide();
        $('#'+3).hide();
        IdfiguraSelect = $(this).val();

        $('#'+IdfiguraSelect).show();


    });
    $.ajax({
        url: pathFigurasAll,
        dataType: "json",
        success: function (data) {
            $.each(data, function (key, value) {
                $("#contenido").append("<tr><td>" + value.nombre + "</td><td>" + value.superficie + "</td>" +
                    "<td id=" + value.base + ">" + value.base + "</td><td>" + value.diametro + "</td><td>" + value.altura + "</td></tr>");

            });

        }
    });
});