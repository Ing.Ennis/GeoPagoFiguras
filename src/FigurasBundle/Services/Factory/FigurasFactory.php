<?php
/**
 * Created by PhpStorm.
 * User: yanet
 * Date: 03/06/17
 * Time: 12:08
 */

namespace FigurasBundle\Services\Factory;
use FigurasBundle\Services\Figuras\Circulo;
use FigurasBundle\Services\Figuras\Cuadrado;
use FigurasBundle\Services\Figuras\Triangulo;
use Exception;
class FigurasFactory
{
    /**
     * Create staticly desired Logger
     *
     * @param string $type Type of Figura to create
     *
     * @return FigurasInterface instance
     */
    static public function createFigura($figura)
    {
        $instance = null;
        switch ($figura['type']) {
            case 'circulo':
                if(isset($figura['radio']))
                    $instance = new Circulo($figura['radio']);
                else
                    throw new Exception('Debe ingresar radio');
                break;

            case 'triangulo':
                if(isset($figura['base']) && isset($figura['altura']))
                    $instance = new Triangulo($figura['base'],$figura['altura']);
                else
                    throw new Exception('Debe ingresar radio');
                break;

            case 'cuadrado':
                if(isset($figura['base']) && isset($figura['altura']))
                    $instance = new Cuadrado($figura['base'],$figura['altura']);
                else
                    throw new Exception('Debe ingresar radio');
                break;
            default:
                throw new Exception('No existe ninguna figura con ese nombre');
                break;
        }

        return $instance;
    }


}