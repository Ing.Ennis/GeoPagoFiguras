<?php
/**
 * Created by PhpStorm.
 * User: yanet
 * Date: 03/06/17
 * Time: 12:29
 */

namespace FigurasBundle\Services\Figuras;


class Circulo implements FigurasInterface
{
    private $superficie;
    private $diametro;
    private $tipoFigura;
    private $radio;

    public function __construct($radio)
    {
        $this->tipoFigura = 'Circulo';
        $this->radio = $radio;
    }

    public function getSuperficie()
    {
        // TODO: Implement Superficie() method.
        /*Formula Pi * radio al cuadrado*/
        $valor = $this->diametro/2;
        $this->superficie = $valor * 3.14;
        return $this->superficie;
    }

    public function getBase()
    {
        // TODO: Implement Base() method.
        /*Suponiendo que se refiere al Area*/
        return null;
    }

    public function getAltura()
    {
        // TODO: Implement Altura() method.
        return null;
    }

    public function getDiametro()
    {
        // TODO: Implement Diametro() method.
        $this->diametro = $this->radio*2;
        return $this->diametro;
    }

    public function getTipoFigura()
    {
        return $this->tipoFigura;
        
        // TODO: Implement TipoFigura() method.
    }
}