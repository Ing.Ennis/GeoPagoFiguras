<?php
/**
 * Created by PhpStorm.
 * User: yanet
 * Date: 03/06/17
 * Time: 12:28
 */

namespace FigurasBundle\Services\Figuras;


class Cuadrado implements FigurasInterface
{
    private $superficie;
    private $base;
    private $altura;
    private $tipoFigura;

    public function __construct($base,$altura)
    {
        $this->tipoFigura = 'Cuadrado';
        $this->base = $base;
        $this->altura = $altura;
    }

    public function getSuperficie()
    {
        // TODO: Implement Superficie() method.
        /*tengo dudas si aqui se refiere al calculo del area*/
        $area = $this->base * $this->altura;
        $this->superficie = $area;
        return $this->superficie;
    }

    public function getBase()
    {
        // TODO: Implement Base() method.
        return $this->base;
    }

    public function getAltura()
    {
        // TODO: Implement Altura() method.
        return $this->altura;
    }

    public function getDiametro()
    {
        // TODO: Implement Diametro() method.
        return null;
    }

    public function getTipoFigura()
    {
        return $this->tipoFigura;
        // TODO: Implement TipoFigura() method.
    }
}