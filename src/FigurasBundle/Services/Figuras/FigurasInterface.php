<?php
/**
 * Created by PhpStorm.
 * User: yanet
 * Date: 03/06/17
 * Time: 12:06
 */

namespace FigurasBundle\Services\Figuras;

interface FigurasInterface
{
    public function getSuperficie();
    public function getBase();
    public function getAltura();
    public function getDiametro();
    public function getTipoFigura();
}