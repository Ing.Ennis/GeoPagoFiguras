<?php
/**
 * Created by PhpStorm.
 * User: yanet
 * Date: 03/06/17
 * Time: 12:29
 */

namespace FigurasBundle\Services\Figuras;


class Triangulo implements FigurasInterface
{
    private $superficie;
    private $base;
    private $altura;
    private $tipoFigura;
    private $diametro;
    private $radio;
    public function __construct($base,$altura)
    {
        $this->tipoFigura = 'Triangulo';
        $this->base = $base;
        $this->altura = $altura;
        $this->getSuperficie();
    }

    public function getSuperficie()
    {
        $this->superficie = $this->base * ($this->altura/2);
        // TODO: Implement Superficie() method.
        return $this->superficie;
    }

    public function getBase()
    {
        // TODO: Implement Base() method.
        return $this->base;
    }

    public function getAltura()
    {
        // TODO: Implement Altura() method.
        return $this->altura;
    }

    public function getDiametro()
    {

        // TODO: Implement Diametro() method.
        return null;
    }

    public function getTipoFigura()
    {
        return $this->tipoFigura;
        // TODO: Implement TipoFigura() method.
    }
}