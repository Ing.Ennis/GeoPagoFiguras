<?php
namespace FigurasBundle\Services\Handle;

/**
 * Created by PhpStorm.
 * User: yanet
 * Date: 03/06/17
 * Time: 17:46
 */
use Doctrine\ORM\EntityManager;
use FigurasBundle\Entity\Figura;

class FiguraService
{
    private $om;

    public function __construct(EntityManager $om)
    {
        $this->om = $om;
    }
    public function addFigura($parametros){

        $typeFigura = $this->om->getRepository('FigurasBundle:TypeFigura')->findOneBy(array('nombre'=>$parametros->getTipoFigura()));
        $figura = new Figura();
        $figura->setAltura($parametros->getAltura());
        $figura->setBase($parametros->getBase());
        $figura->setType($typeFigura);
        $figura->setDiametro($parametros->getDiametro());
        $figura->setSuperficie($parametros->getSuperficie());
        $this->om->persist($figura);
        $this->om->flush();

    }
    public function allFigura(){
        $respuesta = array();
        $figuras = $this->om->getRepository('FigurasBundle:Figura')->findAll();
        foreach ($figuras as $figura){
            $respuesta[] = $figura->getData();
        }
        return $respuesta;

    }

}